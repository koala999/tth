# tth

#### 介绍
使用HTK训练THCHS30-slr的Windows BAT脚本

#### 软件架构

1. go.bat，主脚本
2. config，HTK需要的配置文件
3. dict0，根据THCHS生成的汉语拼音词典
4. proto，HMM原型
5. scripts\，go.bat调用的子脚本

#### 使用说明

1.  修改go.bat文件中的thchs_path变量为正确路径
2.  设置操作系统的PATH变量，确保能正常调用HTK TOOLS命令
3.  运行go.bat