REM 生成词级（拼音）的label文件
REM %%1为thchs30的trn文件所在目录，%%2为生成的mlf文件路径，%%3为flag：空或者0生成词级标注，1生成拼音级标注，2生成音素级标注，%%4为可选的SENT-BEGIN标记，%%5为可选的SENT-END标记

REM 先搜集所有trn文件的内容到临时文件
if exist trns.tmp del trns.tmp
for %%i in (%1\*.trn) do type %%i >> trns.tmp 

set process_line=0

echo #!MLF!# > %2

REM 防止将!MLF!当成变量
setlocal EnableDelayedExpansion

set skip_line=%3
if "!skip_line!" equ "" set skip_line=0

if !skip_line! gtr 2 (
  echo %%3 can not greater than 2
  goto :eof
)


if !skip_line! equ 0 (
  set skip_claus="tokens=1*" 
)  else (
  set skip_claus="skip=!skip_line! tokens=1*" 
)

for /F %%i in (trns.tmp) do (
  echo "*/%%~ni.lab">> %2
  if not "%4" == "" echo %4 >> %2
  set process_line=1

  for /F %skip_claus% %%Y in (%1\%%i) do (
    if !process_line! equ 1 (
      REM 写入第一个token
      echo %%Y>> %2 
      REM 采取递归的方法处理后续tokens
      call scripts\write-tokens.bat "%%Z" %2 
      set process_line=0
    )
  )

  if not "%5" == "" echo %5 >> %2
  echo .>> %2
)

del trns.tmp
