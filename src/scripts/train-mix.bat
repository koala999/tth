REM 第五阶段训练：由单高斯模型到混合高斯模型
REM %%1为模型保存目录，%%2为hmm子目录序号，%%3为训练次数，%%4为GMM的混合数目

if not exist %1 (
  echo FATAL ERROR: %1 not exist.
  goto :eof
)

if not exist %1\hmm%2 (
  echo FATAL ERROR: %1\hmm%2 not exist.
  goto :eof
)

set /a idx=%2+1

echo[  Mixture Incrementing to %4 --^> %1\hmm%idx%
if not exist %1\hmm%idx% md %1\hmm%idx%
echo MU %4 {*.state[2-4].mix}> mkmix.hed
HHEd -H %1\hmm%2\macros -H %1\hmm%2\hmmdefs -M %1\hmm%idx% mkmix.hed data\biphones1 > %1\hmm%idx%\hed.log
del mkmix.hed >nul

call scripts\Do-Rest.bat %1 labels\wintri.mlf data\biphones1 %idx% %3
