@echo off

REM 模型测试
REM %%1为训练模型目录，%%2为词典文件，%%3为模型列表，%%4为测试scp文件，%%5为参考mlf文件，%%6为测试序号，%%7为词网络文件

title Doing-test %6

@echo on

HVite -H %1\macros -H %1\hmmdefs -S %4 -l * -i test\recout%6.mlf -w %7 -p 0.0 -s 5.0 %2 %3

HResults -I %5 %3 test\recout%6.mlf > test\result%6.log

@echo off
type test\result%6.log

REM for /F "skip=5 tokens=*" %%i in (test\result%6.log) do echo[  %%i