REM 根据单词级mlf生成音素级mlf
REM %%1为单词级mlf路径，%%2为词典文件路径，%%3为输出音素级mlf文件路径，%%4为flag，若设置则表示保留sp，未设置表示移除sp

echo EX > mkphones0.led
echo IS sil sil >> mkphones0.led

if "%4" == "" echo DE sp >> mkphones0.led else (
  ::merge "sp sil" to "sil"
  echo ME sil sp sil >> mkphones0.led
)

HLEd -l * -d %2 -i %3 mkphones0.led %1

del mkphones0.led
