REM 第三阶段训练：三音素模型
REM %%1为模型保存目录，%%2为hmm子目录序号，%%3为训练次数

if not exist %1 (
  echo FATAL ERROR: %1 not exist.
  goto :eof
)

if not exist %1\hmm%2 (
  echo FATAL ERROR: %1\hmm%2 not exist.
  goto :eof
)

set /a idx=%2+1

echo[  Making Triphones from Monophones --^> %1\hmm%idx%
echo WB sp > mktri.led
echo WB sil >> mktri.led
echo TC >> mktri.led
HLEd -n data\triphones1 -l * -i labels\wintri.mlf mktri.led labels\phones1.mlf
del mktri.led >nul

if not exist %1\hmm%idx% md %1\hmm%idx%
::自动生成的triphones1不全，使用前面根据字典生成的biphones1进行三音素化
call scripts\make-trihed.bat data\monophones1 data/biphones1 mktri.hed
call HHEd -H %1\hmm%2\macros -H %1\hmm%2\hmmdefs -M %1\hmm%idx% mktri.hed data\monophones1
del mktri.hed >nul

call scripts\Do-Rest.bat %1 labels\wintri.mlf data\biphones1 %idx% %3
