REM 第一阶段训练：单音素模型
REM %%1为模型保存目录，%%2为训练次数，%%3为词网络文件路径

if not exist %1 (
  echo FATAL ERROR: %1 not exist.
  goto :eof
)

echo[  Creating Flat Start Monophones --^> %1\hmm0
if not exist %1\hmm0 md %1\hmm0
HCompV -f 0.01 -m -S data\train.scp -M %1\hmm0 proto

call scripts\make-hmmdefs.bat %1\hmm0\proto %1\hmm0\hmmdefs data\monophones0
call scripts\make-macros.bat %1\hmm0\proto %1\hmm0\macros %1\hmm0\vFloors
del %1\hmm0\proto
del %1\hmm0\vFloors

call scripts\Do-Rest.bat %1 labels\phones0.mlf data\monophones0 0 3
