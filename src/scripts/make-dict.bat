REM 生成发音词典
REM %%1为输入词典，%%2为输出词典目录，%%3为输出音素列表目录

if not exist %2 md %2
if not exist %3 md %3

echo[    Creating basic dict from %1 --^> %2\dict0
::just copy it
type %1 > %2\dict0

echo[    Creating baisc word-netword from basic dict --^> %2\wdnet0
HBuild %2\dict0 %2\wdnet0


echo[    Fixing basic dict with sil ^& sp --^> %2\dict1
echo[    Creating mono-phone list --^> %3\monophones1
::create global.ded
echo AS sp>global.ded
echo MP sil sil sp>>global.ded
  
::create dict.tmp
echo SENT-END [] sil>dict.tmp
echo SENT-START [] sil>>dict.tmp

HDMan -n %3\monophones1 -i %2\dict1 %2\dict0 dict.tmp
del global.ded
del dict.tmp

echo[    Creating mono-phone list wihout sp --^> %3\monophones0
if exist %3\monophones0 del %3\monophones0
for /F %%i in (%3\monophones1) do (
  if not "%%i" equ "sp" echo %%i>>%3\monophones0
)

echo[    Creating word-netword with SENT-START/END --^> %2\wdnet1
HBuild %2\dict1 %2\wdnet1

echo[    Making bi-phone list --^> %3\biphones0
echo sil> %3\biphones0

for /F "tokens=2,3" %%i in (%1) do (
  echo %%i-%%j>>%3\biphones0
  echo %%i+%%j>> %3\biphones0
)

echo[    Making bi-phone list with sp --^> %3\biphones1
type %3\biphones0 > %3\biphones1
echo sp>> %3\biphones1



