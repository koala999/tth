REM 生成macros文件
REM %%1为原型文件路径，%%2为生成的macros文件路径, %%3为vFloors文件路径

if exist %2 del %2
for /F "tokens=1*" %%i in (%1) do (
  if "%%i" == "~h" goto ok
  echo %%i %%j >> %2
)

:ok
type %3 >> %2
