REM 第二阶段训练：加入sil和sp模型训练
REM %%1为模型保存目录，%%2为hmm子目录序号，%%3为训练次数

if not exist %1 (
  echo FATAL ERROR: %1 not exist.
  goto :eof
)

if not exist %1\hmm%2 (
  echo FATAL ERROR: %1\hmm%2 not exist.
  goto :eof
)

setlocal EnableDelayedExpansion
set /a idx1=%2+1
set /a idx2=%2+2

echo[  Fixing the Silence Models
echo[    Appending "sp" Models to hmmdef --^> %1\hmm!idx1!
if not exist %1\hmm!idx1! md %1\hmm!idx1!
call scripts\add-sp-def.bat %1\hmm%2\hmmdefs %1\hmm!idx1!\hmmdefs
type %1\hmm%2\macros > %1\hmm!idx1!\macros
type data\monophones0 > data\monophones1
echo sp>> data\monophones1
echo AT 2 4 0.2 {sil.transP} > sil.hed
echo AT 4 2 0.2 {sil.transP} >> sil.hed
echo AT 1 3 0.3 {sp.transP} >> sil.hed
echo TI silst {sil.state[3],sp.state[2]} >> sil.hed

echo[    Tieing "sp" and "sil" Models --^> %1\hmm!idx2!
if not exist %1\hmm!idx2! md %1\hmm!idx2!
HHEd -H %1\hmm!idx1!/macros -H %1\hmm!idx1!/hmmdefs -M %1\hmm!idx2! sil.hed data\monophones1
del sil.hed

call scripts\Do-Rest.bat %1 labels\phones1.mlf data\monophones1 !idx2! %3
