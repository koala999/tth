REM 第四阶段训练：基于数据驱动聚类的状态绑定
REM %%1为模型保存目录，%%2为hmm子目录序号，%%3为训练次数

if not exist %1 (
  echo FATAL ERROR: %1 not exist.
  goto :eof
)

if not exist %1\hmm%2 (
  echo FATAL ERROR: %1\hmm%2 not exist.
  goto :eof
)

set /a idx=%2+1

echo[  Data-Driven Clustering --^> %1\hmm%idx%
if not exist %1\hmm%idx% md %1\hmm%idx%
echo RO 100.0 %1/hmm%2/stats.txt> mktie.hed
call scripts\make-tiehed.bat data\monophones0 mktie.hed
HHEd -H %1\hmm%2\macros -H %1\hmm%2\hmmdefs -M %1\hmm%idx% mktie.hed data\biphones1 > %1\hmm%idx%\hed.log
del mktie.hed >nul

call scripts\Do-Rest.bat %1 labels\wintri.mlf data\biphones1 %idx% %3
