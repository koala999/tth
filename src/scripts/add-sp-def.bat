REM 生成sp模型定义
REM %%1为输入，包含sil的hmmdef文件路径，%%2为输出，附加了sp的hmmdef文件路径

::先原文复制，然后再追加sp模型定义
copy /y %1 %2 1>nul

setlocal EnableDelayedExpansion

set sil="sil"
set sil_found=0
set state3_found=0

echo ~h "sp" >> %2 
echo ^<BEGINHMM^> >> %2
echo ^<NUMSTATES^> 3 >> %2
echo ^<STATE^> 2 >> %2
 
::提取sil的state3的mean和var参数
for /F "tokens=1*" %%i in (%1) do (
  if !state3_found! gtr 0 (
    if /i "%%i" == "<STATE>" if /i "%%j" == "4" goto trans
    echo %%i %%j >> %2
  ) else if !sil_found! gtr 0 (
    if /i "%%i" == "<STATE>" if /i "%%j" == "3" set state3_found=1
  ) else if "%%j" == "%sil%" set sil_found=1
)

:trans
echo ^<TRANSP^> 3 >> %2
echo 0.0 1.0 0.0 >> %2
echo 0.0 0.5 0.5 >> %2
echo 0.0 0.0 0.0 >> %2
echo ^<ENDHMM^> >> %2

