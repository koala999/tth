REM 迭代调用HRest进行训练
REM %%1为训练模型存取目录，%%2为mlf文件，%%3为hmm-list，%%4为起始hmm子目录序号，%%5为迭代训练次数，%%6为传递给HERest的额外参数

setlocal EnableDelayedExpansion

REM if not exist %1\hmm%4 (
REM   echo FATAL ERROR: %1\hmm%4 not exist.
REM  goto :eof
REM )

set /a ending_idx=%4+%5-1
for /L %%i in (%4 1 %ending_idx%) do (
  set /a idx=%%i+1
  if not exist %1\hmm!idx! md %1\hmm!idx!
  echo[  Re-estimating --^> %1\hmm!idx!
  call HERest %6 -T 7 -I %2 -t 250.0 150.0 1000.0 -S data\train.scp -s %1\hmm!idx!\stats.txt -H %1\hmm%%i\macros -H %1\hmm%%i\hmmdefs -M %1\hmm!idx! %3 > %1\hmm!idx!\rest.log
)
