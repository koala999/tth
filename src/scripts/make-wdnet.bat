REM 创建基于二元文法的词网络
REM %%1为词典文件路径，%%2为用于收集二元关系的词标注文件，%%3为输出的词网络文件路径

if exist wlist.tmp del wlist.tmp
for /F "tokens=1" %%i in (%1) do (
  echo %%i>> wlist.tmp
)

HLStats -b bigfn.tmp -s SENT-START SENT-END -o wlist.tmp %2

HBuild -n bigfn.tmp -s SENT-START SENT-END %1 %3

del wlist.tmp >nul
del bigfn.tmp >nul