REM 根据语料库做的一些数据准备工作
REM %%1为语料库路径，%%2为发音词典路径

echo[  Making the Dictionary ^& Phone-list
if not exist dict md dict
if not exist data md data
call scripts\make-dict.bat dict0 dict data

echo[  Creating the Transcription Files
if not exist labels md labels
echo[    Creating world level transcription file --^> labels\words.mlf
call scripts\make-mlf.bat %1\train labels\words.mlf 1
echo[    Creating phone level transcription file without word-boundary --^> labels\phones0.mlf
call scripts\make-phones.bat labels\words.mlf dict\dict0 %labels\phones0.mlf
echo[    Creating phone level transcription file with sp as word-boundary --^> labels\phones1.mlf
call scripts\make-phones.bat labels\words.mlf dict\dict1 labels\phones1.mlf 1

echo[  Preparing the Coding Script Files --^> data\train.scp
if not exist parms md parms
call scripts\make-train-scp.bat %1\train parms codetr.scp.tmp data\train.scp

echo[  Coding the Data --^> parms
call HCopy -C config -S codetr.scp.tmp
del codetr.scp.tmp >nul

if not "%do_test%" equ "0" (
  if not exist test md test
  echo[  Preparing Test Data --^> test
  call scripts\make-mlf.bat %1\test test\words.mlf 1

  if not exist test\parms md test\parms
  call scripts\make-train-scp.bat %1\test test\parms codetr.scp.tmp test\test.scp
  call HCopy -C config -S codetr.scp.tmp
  del codetr.scp.tmp >nul
)
