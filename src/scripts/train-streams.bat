REM 第六阶段训练：多数据流模型训练
REM %%1为模型保存目录，%%2为hmm子目录序号，%%3为训练次数，%%4为数据流的数目

if not exist %1 (
  echo FATAL ERROR: %1 not exist.
  goto :eof
)

if not exist %1\hmm%2 (
  echo FATAL ERROR: %1\hmm%2 not exist.
  goto :eof
)

set /a idx=%2+1

echo[  Split into %4 independent data streams --^> %1\hmm%idx%
if not exist %1\hmm%idx% md %1\hmm%idx%
echo SS %4 > mkstreams.hed
HHEd -H %1\hmm%2\macros -H %1\hmm%2\hmmdefs -M %1\hmm%idx% mkstreams.hed data\biphones1 > %1\hmm%idx%\hed.log
del mkstreams.hed >nul

call scripts\Do-Rest.bat %1 labels\wintri.mlf data\biphones1 %idx% %3
