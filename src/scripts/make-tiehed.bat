REM 创建用于状态绑定的hed文件
REM %%1为单音素列表文件，%%2为输出hed文件路径

::首先，创建声母和韵母列表文件initial0, finals0
echo[>data\initial0
echo[>data\finals0

for /F "tokens=2,3" %%i in (%1) do (
  find "%%i" data\initial0 >nul
  if !ERRORLEVEL! equ 1 echo %%i>> data\initial0

  find "%%j" data\finals0 >nul
  if !ERRORLEVEL! equ 1 echo %%j>> data\finals0
)

for /F "tokens=*" %%i in (data\initial0) do (
    echo TC 100.0 "%%iS2" {^(*-%%i,%%i+*,*-%%i+*^).state[2]}>> %2
    echo TC 100.0 "%%iS3" {^(*-%%i,%%i+*,*-%%i+*^).state[3]}>> %2
)

for /F "tokens=*" %%i in (data\finals0) do (
    echo TC 100.0 "%%iS3" {^(*-%%i,%%i+*,*-%%i+*^).state[3]}>> %2
    echo TC 100.0 "%%iS4" {^(*-%%i,%%i+*,*-%%i+*^).state[4]}>> %2
)
