REM make a .hed script to clone monophones in a phone list 
REM %%1为monolist，%%2为trilist，%%3为输出hed文件路径

echo CL %2 > %3
for /F %%i in (%1) do (
  if "%%i" neq "" echo TI T_%%i {^(*-%%i+*,%%i+*,*-%%i^).transP} >> %3
)

