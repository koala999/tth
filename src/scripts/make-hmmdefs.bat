REM 生成HMM定义文件
REM %%1为原型文件路径，%%2为生成的mmf文件路径, %%3为phone列表

if exist %2 del %2
if exist proto.tmp del proto.tmp
for /F "skip=4 tokens=*" %%i in (%1) do echo %%i >> proto.tmp
for /F "tokens=1" %%i in (%3) do (
  echo ~h "%%i" >> %2
  type proto.tmp >> %2
)
del proto.tmp
