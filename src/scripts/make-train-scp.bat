REM 生成HCOPY使用的script文件
REM %%1为thchs30的data目录，%%2为HCOPY生成PARM文件目录，%%3为生成scp文件路径，%%4为parm list文件路径

if exist %3 del %3
if exist %4 del %4

for %%i in (%1\*.wav) do (
echo %1\%%~nxi %2\%%~nxi.parm >> %3
echo %2\%%~nxi.parm >> %4
)
