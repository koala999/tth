@echo off

set thchs30_path=D:\data_thchs30
::do_test=0代表不做测试，=1代表只做最终测试，=2代表做过程测试
set do_test=2

::设置为utf-8模式，否则有些分行会出问题
chcp 65001 >nul

cls

echo ==================== Training THCHS30-OPENSLR Using HTK ====================

echo[  PATH: %thchs30_path%
echo[  DATA: %time% %date%


echo --Preparing Data
call scripts\data-prepare.bat %thchs30_path% dict0

if not exist hmms md hmms

echo --Training Monophones
call scripts\train-mono.bat hmms 3
if %do_test% gtr 1 (
  start scripts\Do-Test.bat hmms\hmm3 dict\dict0 data\monophones0 test\test.scp test\words.mlf 0 dict\wdnet0
)

echo --Training Monophones with sil^&sp Models
call scripts\train-sil-sp.bat hmms 3 2
if %do_test% gtr 1 (
  start scripts\Do-Test.bat hmms\hmm7 dict\dict1 data\monophones1 test\test.scp test\words.mlf 1 dict\wdnet1
)

echo --Training Word-Internal Triphones
call scripts\train-tri.bat hmms 7 2 
if %do_test% gtr 1 (
 start scripts\Do-Test.bat hmms\hmm10 dict\dict1 data\biphones1 test\test.scp test\words.mlf 2 dict\wdnet1
)

echo --Training Tied-State Triphones
call scripts\train-tied.bat hmms 10 2
if %do_test% gtr 1 (
  start scripts\Do-Test.bat hmms\hmm13 dict\dict1 data\biphones1 test\test.scp test\words.mlf 3 dict\wdnet1
)

echo --Training Mixtured Triphones
::迭代训练5次
call scripts\train-mix.bat hmms 13 5 3
if %do_test% gtr 1 (
  start scripts\Do-Test.bat hmms\hmm19 dict\dict1 data\biphones1 test\test.scp test\words.mlf 4 dict\wdnet1
)

echo --Training Bigram Word-network
call scripts\make-wdnet.bat dict\dict1 labels\words.mlf dict\wdnet2
if %do_test% gtr 1 (
  start scripts\Do-Test.bat hmms\hmm19 dict\dict1 data\biphones1 test\test.scp test\words.mlf 5 dict\wdnet2
)

echo --Training Mulit-Streams
call scripts\train-streams.bat hmms 19 4 3
if %do_test% gtr 0 (
  start scripts\Do-Test.bat hmms\hmm24 dict\dict1 data\biphones1 test\test.scp test\words.mlf 6 dict\wdnet2
)


echo Finished at %time% %date%
echo[





